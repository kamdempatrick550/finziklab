-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : jeu. 06 jan. 2022 à 20:42
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bdfindzik`
--

-- --------------------------------------------------------

--
-- Structure de la table `album`
--

DROP TABLE IF EXISTS `album`;
CREATE TABLE IF NOT EXISTS `album` (
  `id_album` bigint(20) NOT NULL,
  `annee_de_sortie` int(11) NOT NULL,
  `nom_album` varchar(255) DEFAULT NULL,
  `urlimg` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_album`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `album`
--

INSERT INTO `album` (`id_album`, `annee_de_sortie`, `nom_album`, `urlimg`) VALUES
(4, 2018, 'Ceinture noire', 'resources/covers/cover_gims_ceinture.jpg'),
(5, 2017, 'Le monde a l\'envers', 'resources/covers/cover_zaho_monde.jpg'),
(6, 2016, 'This is acting', 'resources/covers/cover_sia_acting.jpg');

-- --------------------------------------------------------

--
-- Structure de la table `album_artiste`
--

DROP TABLE IF EXISTS `album_artiste`;
CREATE TABLE IF NOT EXISTS `album_artiste` (
  `id_artiste` bigint(20) NOT NULL,
  `id_album` bigint(20) NOT NULL,
  PRIMARY KEY (`id_artiste`,`id_album`),
  UNIQUE KEY `UK_appjpr58lhtv7j5p256qney6j` (`id_album`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `album_artiste`
--

INSERT INTO `album_artiste` (`id_artiste`, `id_album`) VALUES
(1, 5),
(2, 4),
(3, 6);

-- --------------------------------------------------------

--
-- Structure de la table `album_groupe`
--

DROP TABLE IF EXISTS `album_groupe`;
CREATE TABLE IF NOT EXISTS `album_groupe` (
  `id_groupe` bigint(20) NOT NULL,
  `id_album` bigint(20) NOT NULL,
  PRIMARY KEY (`id_groupe`,`id_album`),
  UNIQUE KEY `UK_jm68ao0l0by6y4fe8oegamgk4` (`id_album`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `album_musique`
--

DROP TABLE IF EXISTS `album_musique`;
CREATE TABLE IF NOT EXISTS `album_musique` (
  `id_album` bigint(20) NOT NULL,
  `id_musique` bigint(20) NOT NULL,
  PRIMARY KEY (`id_album`,`id_musique`),
  UNIQUE KEY `UK_px2hjr6cgv1fwddnclrj17sql` (`id_musique`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `album_musique`
--

INSERT INTO `album_musique` (`id_album`, `id_musique`) VALUES
(4, 7),
(4, 8),
(5, 11),
(5, 12),
(6, 9),
(6, 10);

-- --------------------------------------------------------

--
-- Structure de la table `artiste`
--

DROP TABLE IF EXISTS `artiste`;
CREATE TABLE IF NOT EXISTS `artiste` (
  `id_artiste` bigint(20) NOT NULL,
  `nom_artiste` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_artiste`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `artiste`
--

INSERT INTO `artiste` (`id_artiste`, `nom_artiste`) VALUES
(1, 'Zaho'),
(2, 'Maitre Gimx'),
(3, 'Sia');

-- --------------------------------------------------------

--
-- Structure de la table `artiste_groupe`
--

DROP TABLE IF EXISTS `artiste_groupe`;
CREATE TABLE IF NOT EXISTS `artiste_groupe` (
  `id_groupe` bigint(20) NOT NULL,
  `id_artiste` bigint(20) NOT NULL,
  PRIMARY KEY (`id_groupe`),
  UNIQUE KEY `UK_1a8uidmvt48v6dbngiucj356n` (`id_artiste`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

DROP TABLE IF EXISTS `groupe`;
CREATE TABLE IF NOT EXISTS `groupe` (
  `id_groupe` bigint(20) NOT NULL,
  `nom_groupe` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_groupe`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `hibernate_sequence`
--

DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE IF NOT EXISTS `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(13);

-- --------------------------------------------------------

--
-- Structure de la table `musique`
--

DROP TABLE IF EXISTS `musique`;
CREATE TABLE IF NOT EXISTS `musique` (
  `id_musique` bigint(20) NOT NULL,
  `annee_de_sortie` int(11) NOT NULL,
  `categorie` varchar(255) DEFAULT NULL,
  `lyrics` varchar(255) DEFAULT NULL,
  `titre` varchar(255) DEFAULT NULL,
  `urlimg` varchar(255) DEFAULT NULL,
  `urlson` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_musique`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `musique`
--

INSERT INTO `musique` (`id_musique`, `annee_de_sortie`, `categorie`, `lyrics`, `titre`, `urlimg`, `urlson`) VALUES
(7, 2018, 'RAP', '', 'Tant pis', '/static/images/covers/3.jpg', 'resources/audio/Musics/Maitre Gims - Ceinture noire/2 Tant pis.mp3'),
(8, 2018, 'RAP', '', 'Tu ne le vois pas', '/images/gims.jpg', 'resources/audio/Musics/Maitre Gims - Ceinture noire/12 Tu ne le vois pas.mp3'),
(9, 2016, 'POP', '', 'Unstoppable', '/images/cover_sia_acting.jpg', 'resources/audio/Musics/Sia - This Is Acting (2016)/05. Unstoppable.mp3'),
(10, 2016, 'POP', '', 'Sweet Design', '/images/3.jpg', 'resources/audio/Musics/Sia - This Is Acting (2016)/10. Sweet Design.mp3'),
(11, 2017, 'POP', '', 'Tant de choses', '/images/cover_zaho_monde.jpg', 'resources/audio/Musics/Zaho-Le_Monde_A_Lenvers-FR-2017-SO/02-zaho-tant_de_choses.mp3'),
(12, 2017, 'POP', '', 'salamalek', '/images/covers/cover_zaho_monde.jpg', 'resources/audio/Musics/Zaho-Le_Monde_A_Lenvers-FR-2017-SO/06-zaho-salamalek.mp3');

-- --------------------------------------------------------

--
-- Structure de la table `musique_artiste`
--

DROP TABLE IF EXISTS `musique_artiste`;
CREATE TABLE IF NOT EXISTS `musique_artiste` (
  `id_musique` bigint(20) NOT NULL,
  `id_artiste` bigint(20) NOT NULL,
  PRIMARY KEY (`id_artiste`,`id_musique`),
  KEY `FKan70owgeg8oc3bdn5ksfe10r6` (`id_musique`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `musique_artiste`
--

INSERT INTO `musique_artiste` (`id_musique`, `id_artiste`) VALUES
(11, 1),
(12, 1),
(7, 2),
(8, 2),
(9, 3),
(10, 3);

-- --------------------------------------------------------

--
-- Structure de la table `musique_groupe`
--

DROP TABLE IF EXISTS `musique_groupe`;
CREATE TABLE IF NOT EXISTS `musique_groupe` (
  `id_groupe` bigint(20) NOT NULL,
  `id_musique` bigint(20) NOT NULL,
  PRIMARY KEY (`id_groupe`,`id_musique`),
  UNIQUE KEY `UK_7pcqr29nyyhi1vv38x8mo9uag` (`id_musique`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

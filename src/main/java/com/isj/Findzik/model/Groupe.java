package com.isj.Findzik.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString


public class Groupe  {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idGroupe;

    private String nomGroupe;

    @OneToMany
    @JoinTable(name = "Artiste_groupe",
            joinColumns=@JoinColumn( name= "idGroupe"),
            inverseJoinColumns = @JoinColumn( name = "idArtiste"))
    @JsonIgnore
    private Set<Artiste> artistes= new HashSet<>();

    @OneToMany
    @JoinTable(name = "Album_groupe",
            joinColumns=@JoinColumn( name= "idGroupe"),
            inverseJoinColumns = @JoinColumn( name = "idAlbum"))
    @JsonIgnore
    private Set<Album> albums = new HashSet<>();

    @OneToMany
    @JoinTable(name = "Musique_groupe",
            joinColumns=@JoinColumn( name= "idGroupe"),
            inverseJoinColumns = @JoinColumn( name = "idMusique"))
    @JsonIgnore
    private Set<Musique> musiques = new HashSet<>();

}

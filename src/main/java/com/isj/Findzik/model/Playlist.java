package com.isj.Findzik.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class Playlist {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idPlaylist;

    private String pseudoUser;

    private String nomPlaylist;

    @ManyToMany
    @JoinTable(name = "Playlist_Musique",
            joinColumns=@JoinColumn( name= "idPlaylist"),
            inverseJoinColumns = @JoinColumn( name = "idMusique")
    )
    @JsonIgnore
    private Set<Musique> musiques= new HashSet<>();

}

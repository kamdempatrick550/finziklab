package com.isj.Findzik.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Album {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idAlbum;

    private String nomAlbum;

    private int anneeDeSortie;

    private String urlimg;


    @ManyToOne
    @JoinTable(name = "Album_artiste",
            joinColumns=@JoinColumn( name= "idAlbum"),
            inverseJoinColumns = @JoinColumn( name = "idArtiste"))
    private Artiste artiste;

    @OneToMany
    @JoinTable(name = "Album_musique",
            joinColumns=@JoinColumn( name= "idAlbum"),
            inverseJoinColumns = @JoinColumn( name = "idMusique"))
    @JsonIgnore
    private Set<Musique> musiques = new HashSet<>();



    @ManyToOne
    @JoinTable(name = "Album_groupe",
            joinColumns=@JoinColumn( name= "idAlbum"),
            inverseJoinColumns = @JoinColumn( name = "idGroupe"))
    private Groupe groupe;


//    public Album(int i1, String nomAlbum, String s) {
//        this.anneeDeSortie=i1;
//        this.nomAlbum=nomAlbum;
//        this.urlimg=s;
//    }

}

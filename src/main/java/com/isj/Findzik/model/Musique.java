package com.isj.Findzik.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString

public class Musique {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long idMusique;

    private String titre;

    private int annee_de_sortie;

    @Enumerated (EnumType.STRING)
    private Categorie categorie;

    private String lyrics;

    private String urlson;

    private String urlimg;

    @ManyToOne
    @JoinTable(name = "Album_musique",
            joinColumns=@JoinColumn( name= "idMusique"),
            inverseJoinColumns = @JoinColumn( name = "idAlbum"))

    private Album album;

    @ManyToMany
    @JoinTable(name = "Musique_Artiste",
            joinColumns=@JoinColumn( name= "idMusique"),
            inverseJoinColumns = @JoinColumn( name = "idArtiste") )
    @JsonIgnore
    private List<Artiste> artistes;

    @ManyToOne
    @JoinTable(name = "Musique_groupe",
            joinColumns=@JoinColumn( name= "idMusique"),
            inverseJoinColumns = @JoinColumn( name = "idGroupe"))
    private Groupe groupeAuteur;

    @ManyToMany
    @JoinTable(name = "Playlist_Musique",
            joinColumns=@JoinColumn( name= "idMusique"),
            inverseJoinColumns = @JoinColumn( name = "idPlaylist")
    )
    @JsonIgnore
    private Set<Playlist> playlists= new HashSet<>();

    public Musique(String titre, int annee_de_sortie, Categorie categorie, String lyrics, String urlson, String urlimg, Album album) {
        this.titre = titre;
        this.annee_de_sortie = annee_de_sortie;
        this.categorie = categorie;
        this.lyrics = lyrics;
        this.urlson = urlson;
        this.urlimg = urlimg;
        this.album = album;


    }
}

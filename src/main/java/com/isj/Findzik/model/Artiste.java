package com.isj.Findzik.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Artiste {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long idArtiste;

    private String nomArtiste;


    @OneToMany
    @JoinTable(name = "Album_artiste",
            joinColumns=@JoinColumn( name= "idArtiste"),
            inverseJoinColumns = @JoinColumn( name = "idAlbum"))
    @JsonIgnore
    private Set<Album> albums= new HashSet<>();

    @ManyToMany
    @JoinTable(name = "Musique_Artiste",
            joinColumns=@JoinColumn( name= "idArtiste"),
            inverseJoinColumns = @JoinColumn( name = "idMusique") )
    @JsonIgnore
    private List<Musique> musiques = new ArrayList<>();

    @ManyToOne
    @JoinTable(name = "Artiste_groupe",
            joinColumns=@JoinColumn( name= "idArtiste"),
            inverseJoinColumns = @JoinColumn( name = "idGroupe"))

    private Groupe groupe;
}

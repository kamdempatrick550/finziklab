package com.isj.Findzik.service;

import com.isj.Findzik.model.Artiste;
import com.isj.Findzik.model.Musique;

import java.util.List;

public interface ArtisteService {
    List<Artiste> listeArtiste();
    Artiste rechercherArtiste(Long id);
    Artiste enregistrerArtiste(Artiste artiste);
    Artiste modifierArtiste(Artiste artiste);
    void supprimerArtiste(Long id);

    List<Artiste> rechercherParnom(String mot_cle);
}

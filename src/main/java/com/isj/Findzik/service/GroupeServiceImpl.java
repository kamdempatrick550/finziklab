package com.isj.Findzik.service;

import com.isj.Findzik.model.Groupe;
import com.isj.Findzik.repository.GroupeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GroupeServiceImpl implements GroupeService {

    @Autowired
    private GroupeRepository groupeRepository;

    @Override
    public List<Groupe> listeGroupe() {
        return groupeRepository.findAll();
    }

    @Override
    public Groupe rechercherGroupe(Long id) {
        return groupeRepository.findById(id).get();
    }

    @Override
    public Groupe enregistrerGroupe(Groupe groupe) {
        return groupeRepository.save(groupe);
    }

    @Override
    public Groupe modifierGroupe(Groupe groupe) {
        return null;
    }

    @Override
    public void supprimerGroupe(Long id) {
        groupeRepository.deleteById(id);
    }

    @Override
    public List<Groupe> rechercherParnom(String mot_cle) {
        return groupeRepository.findAllByNomGroupeContaining(mot_cle);
    }
}

package com.isj.Findzik.service;

import com.isj.Findzik.model.Artiste;
import com.isj.Findzik.repository.ArtisteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArtisteServiceImpl implements ArtisteService {

    @Autowired
    private ArtisteRepository artisteRepository;

    @Override
    public List<Artiste> listeArtiste() {
        return artisteRepository.findAll();
    }

    @Override
    public Artiste rechercherArtiste(Long id) {
        return artisteRepository.findById(id).get();
    }

    @Override
    public Artiste enregistrerArtiste(Artiste artiste) {
        return artisteRepository.save(artiste);
    }

    @Override
    public Artiste modifierArtiste(Artiste artiste) {
        return artisteRepository.save(artiste);
    }

    @Override
    public void supprimerArtiste(Long id) {
        artisteRepository.deleteById(id);
    }

    @Override
    public List<Artiste> rechercherParnom(String mot_cle) {
        return artisteRepository.findAllByNomArtisteContaining(mot_cle);
    }


}

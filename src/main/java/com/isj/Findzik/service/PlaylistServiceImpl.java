package com.isj.Findzik.service;

import com.isj.Findzik.model.Playlist;
import com.isj.Findzik.repository.PlaylistRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PlaylistServiceImpl implements PlaylistService {

    @Autowired
    private PlaylistRepository playlistRepository;


    @Override
    public List<Playlist> listePlaylist() {
        return playlistRepository.findAll();
    }

    @Override
    public Playlist rechercherPlaylist(Long id) {
        return playlistRepository.findById(id).get();
    }

    @Override
    public Playlist enregistrerPlaylist(Playlist musique) {
        return playlistRepository.save(musique);
    }

    @Override
    public Playlist modifierPlaylist(Playlist musique) {
        return null;
    }

    @Override
    public void supprimerPlaylist(Long id) {
         playlistRepository.deleteById(id);
    }

    @Override
    public Playlist LoginPlaylist(String pseudo, String nom_Playlist) {
        return playlistRepository.findByPseudoUserAndNomPlaylist(pseudo,nom_Playlist);
    }
}

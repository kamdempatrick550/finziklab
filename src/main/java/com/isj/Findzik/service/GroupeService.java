package com.isj.Findzik.service;

import com.isj.Findzik.model.Groupe;

import java.util.List;

public interface GroupeService {

    List<Groupe> listeGroupe();
    Groupe rechercherGroupe(Long id);
    Groupe enregistrerGroupe(Groupe groupe);
    Groupe modifierGroupe(Groupe groupe);
    void supprimerGroupe(Long id);

    List<Groupe> rechercherParnom(String mot_cle);

}

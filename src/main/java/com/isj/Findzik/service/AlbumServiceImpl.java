package com.isj.Findzik.service;

import com.isj.Findzik.model.Album;
import com.isj.Findzik.repository.AlbumRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AlbumServiceImpl implements AlbumService {

    @Autowired
    private AlbumRepository albumRepository;

    @Override
    public List<Album> listeAlbum() {
        return albumRepository.findAll();
    }

    @Override
    public Album rechercherAlbum(Long id) { return albumRepository.findById(id).get(); }


    @Override
    public Album enregistrerAlbum(Album album) {
        return albumRepository.save(album);
    }

    @Override
    public Album modifierAlbum(Album album) {
        return albumRepository.save(album);
    }

    @Override
    public void supprimerAlbum(Long id) {
        albumRepository.deleteById(id);
    }

    @Override
    public List<Album> rechercherParnom(String mot_cle) {
        return albumRepository.findAllByNomAlbumContaining(mot_cle);
    }

    @Override
    public List<Album> rechercherParAnnee(int mot_cle) {
        return albumRepository.findAllByAnneeDeSortieEquals(mot_cle);
    }

    @Override
    public List<Album> rechercherParArtiste(String mot_cle) {
        return albumRepository.findAllByArtiste_NomArtisteContaining(mot_cle);
    }

    @Override
    public List<Album> rechercherParGroupe(String mot_cle) {
        return albumRepository.findAllByArtiste_NomArtisteContaining(mot_cle);
    }
}

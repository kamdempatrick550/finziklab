package com.isj.Findzik.service;

import com.isj.Findzik.model.Album;

import java.util.List;

public interface AlbumService {

    List<Album> listeAlbum();
    Album rechercherAlbum(Long id);
    Album enregistrerAlbum(Album album);
    Album modifierAlbum(Album album);
    void supprimerAlbum(Long id);

    List<Album> rechercherParnom(String mot_cle);
    List<Album> rechercherParAnnee(int mot_cle);
    List<Album> rechercherParArtiste(String mot_cle);
    List<Album> rechercherParGroupe(String mot_cle);
}

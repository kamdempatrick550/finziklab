package com.isj.Findzik.service;

import com.isj.Findzik.model.Suggestion;
import com.isj.Findzik.repository.SuggestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SuggestionServiceImpl implements SuggestionService {

    @Autowired
    private SuggestionRepository suggestionRepository;

    @Override
    public List<Suggestion> listeSuggestion() {
        return suggestionRepository.findAll();
    }

    @Override
    public Suggestion rechercherSuggestion(Long id) {
        return suggestionRepository.findById(id).get();
    }

    @Override
    public Suggestion enregistrerSuggestion(Suggestion suggestion) {
        return suggestionRepository.save(suggestion);
    }

    @Override
    public Suggestion modifierSuggestion(Suggestion suggestion) {
        return null;
    }

    @Override
    public void supprimerSuggestion(Long id) {
        suggestionRepository.deleteById(id);
    }
}

package com.isj.Findzik.service;

import com.isj.Findzik.model.Categorie;
import com.isj.Findzik.model.Musique;

import java.util.List;
import java.util.Set;

public interface MusiqueService {
    List<Musique> listeMusique();
    Musique rechercherMusique(Long id);
    Musique enregistrerMusique(Musique musique);
    Musique modifiermusique(Musique musique);
    void supprimerMusique(Long id);

    List<Musique> rechercheMusiqueParTitre(String mot_cle);
    List<Musique> rechercheMusiqueParCategorie(Categorie mot_cle);

    List<Musique> rechercheMusiqueParNom2(Set<Musique> musiques, String mot_cle);
}


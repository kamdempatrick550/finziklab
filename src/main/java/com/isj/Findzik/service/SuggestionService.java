package com.isj.Findzik.service;

import com.isj.Findzik.model.Suggestion;

import java.util.List;

public interface SuggestionService {

    List<Suggestion> listeSuggestion();
    Suggestion rechercherSuggestion(Long id);
    Suggestion enregistrerSuggestion(Suggestion suggestion);
    Suggestion modifierSuggestion(Suggestion suggestion);
    void supprimerSuggestion(Long id);
}

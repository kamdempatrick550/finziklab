package com.isj.Findzik.service;

import com.isj.Findzik.model.Playlist;

import java.util.List;

public interface PlaylistService {

    List<Playlist> listePlaylist();
    Playlist rechercherPlaylist(Long id);
    Playlist enregistrerPlaylist(Playlist musique);
    Playlist modifierPlaylist(Playlist musique);
    void supprimerPlaylist(Long id);

    Playlist LoginPlaylist(String pseudo,String nom_Playlist);
}

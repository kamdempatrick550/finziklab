package com.isj.Findzik.service;

import com.isj.Findzik.model.Categorie;
import com.isj.Findzik.model.Musique;
import com.isj.Findzik.repository.MusiqueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


@Service
public class MusiqueServiceImpl implements MusiqueService {

    @Autowired
    private MusiqueRepository musiqueRepository;

    @Override
    public List<Musique> listeMusique() {
        return musiqueRepository.findAll();
    }

    @Override
    public Musique rechercherMusique(Long id) {
        return musiqueRepository.findById(id).get();
    }

    @Override
    public Musique enregistrerMusique(Musique musique) {
        return musiqueRepository.save(musique);
    }

    @Override
    public Musique modifiermusique(Musique musique) {
        return musiqueRepository.save(musique);
    }

    @Override
    public void supprimerMusique(Long id) {
            musiqueRepository.deleteById(id);
    }

    @Override
    public List<Musique> rechercheMusiqueParTitre(String mot_cle) {
        return musiqueRepository.findAllByTitreContaining(mot_cle);
    }

    @Override
    public List<Musique> rechercheMusiqueParCategorie(Categorie mot_cle) {
        return musiqueRepository.findAllByCategorie(mot_cle);
    }

    @Override
    public List<Musique> rechercheMusiqueParNom2(Set<Musique> musiques, String mot_cle) {

        List <Musique> musiques1=new ArrayList<>();
        for (Musique m:musiques){
            if (m.getTitre().equals(mot_cle)){
               musiques1.add(m);
            }
        }

        return musiques1;
    }

}

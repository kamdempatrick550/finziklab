package com.isj.Findzik.repository;

import com.isj.Findzik.model.Categorie;
import com.isj.Findzik.model.Musique;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MusiqueRepository extends JpaRepository<Musique,Long> {

//     @Query(value = "SELECT m FROM musique WHERE titre LIKE :mot_cle")
//     List<Musique> Rechercher(@Param("mot_cle")String mot_cle);


    List<Musique> findAllByTitreContaining(String mot_cle);

    List<Musique> findAllByCategorie(Categorie mot_cle);




}

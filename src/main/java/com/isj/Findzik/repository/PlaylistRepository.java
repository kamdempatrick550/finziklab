package com.isj.Findzik.repository;

import com.isj.Findzik.model.Playlist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PlaylistRepository extends JpaRepository<Playlist, Long> {

    Playlist findByPseudoUserAndNomPlaylist(String pseudo,String nom_Playlist);


}

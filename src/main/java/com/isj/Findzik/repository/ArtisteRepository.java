package com.isj.Findzik.repository;

import com.isj.Findzik.model.Artiste;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ArtisteRepository extends JpaRepository<Artiste, Long> {

    List<Artiste> findAllByNomArtisteContaining(String mot_cle);

}

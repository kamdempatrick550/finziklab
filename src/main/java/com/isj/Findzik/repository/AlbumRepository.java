package com.isj.Findzik.repository;

import com.isj.Findzik.model.Album;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AlbumRepository extends JpaRepository<Album, Long> {
        List<Album> findAllByNomAlbumContaining(String mot_cle);
        List<Album> findAllByAnneeDeSortieEquals(int mot_cle);
        List<Album> findAllByArtiste_NomArtisteContaining(String mot_cle);
        List<Album> findAllByGroupe_NomGroupeContaining(String mot_cle);
}

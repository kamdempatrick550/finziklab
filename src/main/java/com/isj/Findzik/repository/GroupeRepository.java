package com.isj.Findzik.repository;

import com.isj.Findzik.model.Groupe;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupeRepository extends JpaRepository<Groupe,Long> {


    List<Groupe> findAllByNomGroupeContaining(String mot_cle);
}

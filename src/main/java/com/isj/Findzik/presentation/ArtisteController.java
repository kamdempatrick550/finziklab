package com.isj.Findzik.presentation;


import com.isj.Findzik.model.Artiste;
import com.isj.Findzik.model.Groupe;
import com.isj.Findzik.model.Musique;
import com.isj.Findzik.service.ArtisteService;
import com.isj.Findzik.service.GroupeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("artiste")
public class ArtisteController {

    @Autowired
    private ArtisteService artisteService;

    @Autowired
    private GroupeService groupeService;


    @GetMapping("/Artiste")
    public String listeArtiste(Model model){
        List<Artiste> artistes = artisteService.listeArtiste();
        List<Groupe> groupes =groupeService.listeGroupe();

        model.addAttribute("artistes",artistes);
        model.addAttribute("groupes",groupes);

        return "blogA";
    }

    @PostMapping("/rechercher")
    public String rechercherArtiste(Model model, @RequestParam(value = "mot_cle") String mot_cle){
//        Set<Artiste> artistes=model.getAttribute("artistes");
        List<Artiste> artistes = artisteService.rechercherParnom(mot_cle);
        List<Groupe> groupes =groupeService.rechercherParnom(mot_cle);
//        Set<Artiste> artisteSet=new HashSet<>();
//        for (Artiste a:
//             artistes) {
//            if (a.getNomArtiste().equals(mot_cle)){
//                artisteSet.add(a);
//            }
//        }
        model.addAttribute("artistes",artistes);
        model.addAttribute("groupes",groupes);
        return "blogA";
    }


}

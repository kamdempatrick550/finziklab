package com.isj.Findzik.presentation;

import com.isj.Findzik.model.Suggestion;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.isj.Findzik.service.SuggestionService;
import org.springframework.beans.factory.annotation.Autowired;

@Controller
@RequestMapping("suggestion")
public class SuggestionController {

    @Autowired
    private SuggestionService suggestionService;

    @GetMapping("/suggestions")
    public String affiche(Model model){
        Suggestion suggestionForm =  new Suggestion();

        model.addAttribute("suggestionForm",suggestionForm);
        return "contact";
    }


    @PostMapping("/enregistrerSuggestion")
    public String enregistrerPlaylist(@ModelAttribute Suggestion suggestionForm) {
        // appel de la couche service injectée pour enregistrer une playlist
        suggestionService.enregistrerSuggestion(suggestionForm);

        return "redirect:suggestions";
    }


}


package com.isj.Findzik.presentation;


import com.isj.Findzik.model.*;
import com.isj.Findzik.service.AlbumService;
import com.isj.Findzik.service.ArtisteService;
import com.isj.Findzik.service.GroupeService;
import com.isj.Findzik.service.MusiqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("musique")
public class MusiqueController {

    @Autowired
    private MusiqueService musiqueService;

    @Autowired
    private ArtisteService artisteService;

    @Autowired
    private AlbumService albumService;

    @Autowired
    private GroupeService groupeService;


    @GetMapping("/")
    public String accueil(Model model){
        return "index";
    }


    @GetMapping("/musique")
    public String listeMusique(Model model){
        List<Musique> musiques = musiqueService.listeMusique();

        model.addAttribute("musiques",musiques);

        return "blog";
    }

    @GetMapping("/supprime")
    public String supprimeMusique(@RequestParam(value = "id") String id){

        long idmusique = Long.parseLong(id);

        musiqueService.supprimerMusique(idmusique);

        return "redirect:blog";
    }


    @GetMapping("/rechercher")
    public String rechercherMusique(@RequestParam(value = "mot_cle") String mot_cle, Model model ){

        Set<Musique> musiques= new HashSet<>();
        List<Musique> musique_titre = musiqueService.rechercheMusiqueParTitre(mot_cle);
        List<Musique> musique_liste = musiqueService.listeMusique();

            for (Musique m: musique_liste){
                if((mot_cle.toUpperCase()).equals(m.getCategorie().toString())){
                    musiques.add(m);
                }
            }
        musiques.addAll(musique_titre);

        for (Artiste artiste: artisteService.rechercherParnom(mot_cle)){
            musiques.addAll(artiste.getMusiques());
        }
        for (Album album:albumService.rechercherParnom(mot_cle)) {
            musiques.addAll(album.getMusiques());
        }
        for (Groupe groupe:groupeService.rechercherParnom(mot_cle)) {
            musiques.addAll(groupe.getMusiques());
        }


        //List<Musique> musiques = musiqueService.rechercheMusiqueParTitre(mot_cle);
        model.addAttribute("musiques",musiques);

        return "blog";
    }


    //Rechercher par categorie
    //ICI LA C COMPLIQUE DEH. JE SUIS PAS SUR
    @GetMapping("/parcat")
    public String classementCat(@RequestParam(value = "categorie") String cat, Model model){
        List<Musique> musiques = musiqueService.listeMusique();
        Categorie[] categories= Categorie.values();
        Set<Musique> musiqueCat=new HashSet<>();
        Categorie categorie= null;
//        for (Categorie c:
//             categories) {
//            if(c.toString().equals(cat)){
//                categorie=c;
//            }
//        }
        for (Categorie c:
             categories) {
            musiqueCat.clear();
            for (Musique musique:
                    musiques) {
                if (musique.getCategorie()==c){
                    musiqueCat.add(musique);
                }
            }
            model.addAttribute("musiqueCat"+c,musiqueCat);

        }

            model.addAttribute("categories",categories);

            return "";

    }

    //recherche specifique a une page
    @GetMapping("/rechercherSpecifique")
    public String rechercherMusiqueSpecifique(@RequestParam(value = "mot_cle") String mot_cle, Model model ){

        Set<Musique> musiques= new HashSet<>();
        Set<Musique> musique_liste = (Set<Musique>) model.getAttribute("musiques");//ATTENTION. en tt cas si ca cuit je remplace le type de retour par object et je change le param de la fonction de recherche

        musiques.addAll(musiqueService.rechercheMusiqueParNom2(musique_liste,mot_cle));

        //List<Musique> musiques = musiqueService.rechercheMusiqueParTitre(mot_cle);
        model.addAttribute("musiques",musiques);

        return "blog";
    }

}

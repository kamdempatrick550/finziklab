package com.isj.Findzik.presentation;


import com.isj.Findzik.model.Musique;
import com.isj.Findzik.model.Playlist;
import com.isj.Findzik.service.MusiqueService;
import com.isj.Findzik.service.PlaylistService;
import com.isj.Findzik.service.PlaylistServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("playlist")
public class PlaylistController  {

    @Autowired
    private PlaylistService playlistService;
    @Autowired
    private MusiqueService musiqueService;

    @GetMapping("loginPlaylist")
    public String liste_playlist(@ModelAttribute Playlist playlistForm, Model model){
        Playlist playlist = playlistService.LoginPlaylist(playlistForm.getPseudoUser(),playlistForm.getNomPlaylist());
        Set<Musique> musiques= playlist.getMusiques();
        model.addAttribute("musiques",musiques);

        return "blog";
    }

    @GetMapping("loginpage")
    public String longinPage(Model model){
        Playlist playlistForm=new Playlist();

        model.addAttribute("playlistForm",playlistForm);
        return "form-login";
    }

//    @PostMapping("enregistrerPlaylist")
//    public String enregistrerPlaylist(@ModelAttribute Playlist playlistForm){
//        playlistService.enregistrerMusique(playlistForm);
//        return "";
//    }

    @GetMapping("/formenregistre")
    public String formulaireEnregistre(Model model){

        Playlist playlistForm =  new Playlist();


        model.addAttribute("playlistForm",playlistForm);

        return "form-playlist";
    }

    @PostMapping("/enregistrerPlaylist")
    public String enregistrerPlaylist(@ModelAttribute Playlist playlistForm) {
        // appel de la couche service injectée pour enregistrer une playlist

        playlistService.enregistrerPlaylist(playlistForm);

        return "redirect:http://localhost:8080/musique/musique";
    }

    @PostMapping("/modifierPlaylist")
    public String modifierPlaylist(@ModelAttribute Playlist playlistForm) {
        // appel de la couche service injectée pour enregistrer une playlist
        Playlist playlist = playlistService.LoginPlaylist(playlistForm.getPseudoUser(),playlistForm.getNomPlaylist());
        playlist.getMusiques().addAll(playlistForm.getMusiques());
        System.out.println(playlistForm.getMusiques().size());
        playlistService.enregistrerPlaylist(playlist);

        return "redirect:http://localhost:8080/musique/musique";
    }

    @GetMapping("/formajout")
    public String formulaireAjoutMus(Model model,@RequestParam(value = "id") String id){
        Long idLong=Long.parseLong(id);
        Playlist playlistForm =  new Playlist();
//        playlistForm.getMusiques().add(musiqueService.rechercherMusique(idLong));
//        System.out.println(playlistForm.getMusiques().size());
        model.addAttribute("playlistForm",playlistForm);
        model.addAttribute("Mus",musiqueService.rechercherMusique(idLong));

        return "ajoutMus";
    }

//    @GetMapping("/ajouterSon")
//    public String ajoutSonPlaylist(@ModelAttribute Playlist playlistForm,Model model){
//        playlistForm
//    }


}

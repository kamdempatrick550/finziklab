package com.isj.Findzik.presentation;


import com.isj.Findzik.model.Album;
import com.isj.Findzik.model.Musique;
import com.isj.Findzik.service.AlbumService;
import com.isj.Findzik.service.MusiqueService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("album")
public class AlbumController {

    @Autowired
    private AlbumService albumService;

    @Autowired
    private MusiqueService musiqueService;

    @GetMapping("/index")
    public String accueil(Model model){
        return "index";
    }

    @GetMapping("/listAlbum")
    public String listeAlbum(Model model){
        List<Album> albums = albumService.listeAlbum();

        model.addAttribute("albums",albums);

        return "album";
    }

    @GetMapping("/sons")
    public String sonsAlbum(@RequestParam(value = "id") String id, Model model){

        long idalbum = Long.parseLong(id);

        Album album = albumService.rechercherAlbum(idalbum);

        Set<Musique> musiques;

        musiques=album.getMusiques();

        model.addAttribute("musiques",musiques);

        return "blog";
    }

    @GetMapping("/recherche")
    public String rechercherMusique(@RequestParam(value = "mot_cle") String mot_cle, Model model ){
        List<Album> albums = albumService.rechercherParnom(mot_cle);

        model.addAttribute("albums",albums);

        return "album";


    }

    @GetMapping("/rechercheAnnee")
    public String rechercherMusiqueAnnee(@RequestParam(value = "mot_cle") String mot_cle, Model model ){
        List<Album> albums=albumService.rechercherParAnnee(Integer.parseInt(mot_cle));
        model.addAttribute("albums",albums);

        return "album";
    }

    @GetMapping("/rechercheArtiste")
    public String rechercherMusiqueArtiste(@RequestParam(value = "mot_cle") String mot_cle, Model model ){
        Set<Album> albumSet=new HashSet<>();
        List<Album> albums=albumService.rechercherParArtiste(mot_cle);
        albums.addAll(albumService.rechercherParGroupe(mot_cle));
        albumSet.addAll(albums);
        model.addAttribute("albums",albumSet);

        return "album";
    }

    }

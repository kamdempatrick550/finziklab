package com.isj.Findzik.presentation.api;


import com.isj.Findzik.model.Album;
import com.isj.Findzik.model.Artiste;
import com.isj.Findzik.service.AlbumService;
import com.isj.Findzik.service.ArtisteService;
import com.isj.Findzik.service.MusiqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/artiste/api/")

public class ArtisteControllerApi {

    @Autowired
    private MusiqueService musiqueService;

    @Autowired
    private AlbumService albumService;

    @Autowired
    private ArtisteService artisteService;


    @PostMapping("enregitrerartiste")
    public String enregistrer_artiste(){
        String[] art={"Zaho","Maitre Gimx"};

            Artiste artiste=new Artiste();
            artiste.setNomArtiste("Sia");
            artisteService.enregistrerArtiste(artiste);


        return "ok";

    }

    @PostMapping("modifierartiste")
    public String modifier_artiste(){

        Artiste artiste=artisteService.rechercherArtiste((long) 3);

        artiste.getMusiques().add(musiqueService.rechercherMusique((long) 10));


         artisteService.modifierArtiste(artiste);

        Artiste artiste2=artisteService.rechercherArtiste((long) 1);

        artiste2.getMusiques().add(musiqueService.rechercherMusique((long) 11));


        artisteService.modifierArtiste(artiste2);
        Artiste artiste3=artisteService.rechercherArtiste((long) 1);

        artiste3.getMusiques().add(musiqueService.rechercherMusique((long) 12));


        artisteService.modifierArtiste(artiste3);


       /* Artiste artiste2=artisteService.rechercherArtiste((long) 2);

        artiste2.getAlbums().add(albumService.rechercherAlbum((long) 4));

        artisteService.modifierArtiste(artiste2);


        Artiste artiste3=artisteService.rechercherArtiste((long) 3);

        artiste3.getAlbums().add(albumService.rechercherAlbum((long) 6));

        artisteService.modifierArtiste(artiste3);*/

        return "ok";


    }

    @GetMapping("listeartiste")
    public List<Artiste> lister_album(){
        return artisteService.listeArtiste();
    }
}

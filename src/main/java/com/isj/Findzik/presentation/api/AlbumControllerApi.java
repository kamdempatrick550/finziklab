package com.isj.Findzik.presentation.api;


import com.isj.Findzik.model.Album;
import com.isj.Findzik.model.Musique;
import com.isj.Findzik.service.AlbumService;
import com.isj.Findzik.service.ArtisteService;
import com.isj.Findzik.service.MusiqueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("album/api/")
public class AlbumControllerApi {

    @Autowired
    private MusiqueService musiqueService;

    @Autowired
    private AlbumService albumService;

    @Autowired
    private ArtisteService artisteService;

    @PostMapping("enregistreralbum")
    public Album enregistrerAlbum(@RequestBody Album album){


        return albumService.enregistrerAlbum(album);
    }

    @PostMapping("modifieralbum")
    public String modifier_album(){

        Album album1=albumService.rechercherAlbum((long) 4);
        album1.setArtiste(artisteService.rechercherArtiste((long) 2));
        albumService.enregistrerAlbum(album1);

        Album album2=albumService.rechercherAlbum((long) 6);
        album2.setArtiste(artisteService.rechercherArtiste((long) 3));
        albumService.enregistrerAlbum(album2);

        Album album3=albumService.rechercherAlbum((long) 5);
        album3.setArtiste(artisteService.rechercherArtiste((long) 1));
        albumService.enregistrerAlbum(album3);
        return "ok";
    }

    @GetMapping("listealbum")
    public List<Album> lister_album(){
        return albumService.listeAlbum();
    }

    @GetMapping("sons")
    public Set<Musique> sonsAlbum(@RequestParam(value = "id") Long id) {

        Album album = albumService.rechercherAlbum(id);
        return album.getMusiques();
    }


}



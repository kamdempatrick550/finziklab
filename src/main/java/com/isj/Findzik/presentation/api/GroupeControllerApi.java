package com.isj.Findzik.presentation.api;

import com.isj.Findzik.model.Groupe;
import com.isj.Findzik.service.GroupeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/groupe/api/")

public class GroupeControllerApi {
    @Autowired
    private GroupeService groupeService;

    @PostMapping("enregistrer")
    public String enregistrerGroupe(@RequestBody Groupe groupe){
        groupeService.enregistrerGroupe(groupe);
        return "ok";

    }
    @GetMapping("liste")
    public List<Groupe> lister_groupe(){
        return groupeService.listeGroupe();
    }


}

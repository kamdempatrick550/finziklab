package com.isj.Findzik.presentation.api;


import com.isj.Findzik.model.Artiste;
import com.isj.Findzik.model.Categorie;
import com.isj.Findzik.model.Musique;
import com.isj.Findzik.model.Playlist;
import com.isj.Findzik.service.AlbumService;
import com.isj.Findzik.service.ArtisteService;
import com.isj.Findzik.service.MusiqueService;
import com.isj.Findzik.service.PlaylistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("musique/api/")
public class MusiqueControllerApi {

    @Autowired
    private  MusiqueService musiqueService;

    @Autowired
    private AlbumService albumService;

    @Autowired
    private ArtisteService artisteService;

    @Autowired
    private PlaylistService playlistService;

    @PostMapping("enregistrermusique")
    public Musique enregistrerMusique(@RequestBody Musique musique){
        return musiqueService.enregistrerMusique(musique);

    }
    @GetMapping("listemusique")
    public List<Musique> lister_album(){
        return musiqueService.listeMusique();
    }

    @GetMapping("listePlaylist/{id}")
    public Set<Musique> listeMus(@PathVariable long id){
        Playlist playlist=playlistService.rechercherPlaylist(id);
        return playlist.getMusiques();
    }

}
